//
//  ViewController.m
//  NetworkingRequestsDemo
//
//  Created by James Cash on 22-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDelegate,UITableViewDataSource>

@property (strong,nonatomic) NSArray<NSString*>* statusCodes;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.statusCodes = @[@"100",
                         @"200",
                         @"201",
                         @"202",
                         @"203"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.statusCodes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"statusCell"];

    UILabel *label = [cell viewWithTag:1];

    label.text = self.statusCodes[indexPath.row];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *img = [cell viewWithTag:2];

    NSString *statusCode = self.statusCodes[indexPath.row];
    NSURL *imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://httpstatusdogs.com/img/%@.jpg", statusCode]];

    // DON'T DO THIS!!!
    //img.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imgURL]];
    // this is kind of a manual alternative to using NSURLSessions
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperationWithBlock:^{
        UIImage *downloadImg = [UIImage imageWithData:[NSData dataWithContentsOfURL:imgURL]];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            img.image = downloadImg;
        }];
    }];
}

@end
